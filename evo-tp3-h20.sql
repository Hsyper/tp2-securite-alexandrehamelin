-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  sam. 11 avr. 2020 à 19:13
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `evo-tp3-h20`
--

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

DROP TABLE IF EXISTS `commentaires`;
CREATE TABLE IF NOT EXISTS `commentaires` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  `prenom` text NOT NULL,
  `commentaire` varchar(255) NOT NULL,
  `livre_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commentaires`
--

INSERT INTO `commentaires` (`id`, `nom`, `prenom`, `commentaire`, `livre_id`) VALUES
(22, 'Fleury', 'Louis', 'J\'ai vraiment détesté ce livre. ', 1),
(31, 'Hénault', 'Monique', 'Les dessins sont juste trop malades!', 1);

-- --------------------------------------------------------

--
-- Structure de la table `livres`
--

DROP TABLE IF EXISTS `livres`;
CREATE TABLE IF NOT EXISTS `livres` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `isbn` bigint(20) UNSIGNED NOT NULL,
  `titre` text NOT NULL,
  `nomAuteur` text NOT NULL,
  `prenomAuteur` text NOT NULL,
  `description` text NOT NULL,
  `categorie` tinyint(3) UNSIGNED NOT NULL,
  `genre` tinyint(3) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `livres`
--

INSERT INTO `livres` (`id`, `isbn`, `titre`, `nomAuteur`, `prenomAuteur`, `description`, `categorie`, `genre`, `created_at`, `updated_at`) VALUES
(1, 1234567891234, 'Moi ce que j\'aime c\'est les monstres', 'dsfsdf', 'sdfsdf', 'Chicago, fin des années 1960. Karen Reyes, dix ans, admire les fantômes, les vampires et autres\r\n            morts-vivants. Elle s’imagine même être un loup-garou: plus facile, ici, d’être un monstre que d’être une\r\n            femme. Le jour de la Saint-Valentin, sa séduisante voisine, Anka Silverberg, se suicide d’une balle dans\r\n            le coeur. Mais Karen n’y croit pas et décide d’élucider cette mort suspecte. Elle va vite découvrir qu’entre\r\n            le passé d’Anka dans l’Allemagne nazie, son propre quartier prêt à s’embraser et les drames tapis dans\r\n            l’ombre de son quotidien, les monstres, bons ou mauvais, sont des êtres comme les autres, ambigus,\r\n            torturés et fascinants.Journal intime d’une artiste prodige, Moi, ce que j’aime, c’est les monstres est un\r\n            kaléidoscope brillant d’énergie et d’émotions, l’histoire magnifiquement contée d’une fascinante enfant au\r\n            coeur du Chicago en ébullition des années 1960. Dans cette oeuvre magistrale, tout à la fois enquête, drame\r\n            familial et témoignage historique, <a href=\"#\" class=\"auteur\">Emil Ferris</a> tisse un lien infiniment\r\n            personnel entre un expressionnisme\r\n            féroce, les hachures d’un <a href=\"#\" class=\"auteur\">Crumb</a> et l’univers de <a href=\"#\" class=\"auteur\">Maurice\r\n              Sendak</a> (Max et les maximonstres).', 2, 2, '2020-03-09 20:22:45', '2020-04-11 14:13:17'),
(2, 1234567891234, '23 secrets bien gardés', 'sdffsd', 'sdfsdf', 'dsfsdf', 2, 2, '2020-03-09 20:22:45', '2020-03-12 09:35:37'),
(3, 1234567891234, 'The Crimes of Grindelwald', 'werwer', 'werwer', 'werwer wer we', 2, 2, '2020-03-09 20:22:45', '2020-03-12 09:35:37'),
(4, 1234567891234, 'Astérix chez les Québécois', '123123', '123123', '123123', 3, 2, '2020-03-09 20:23:07', '2020-03-12 09:35:37'),
(5, 1234567891234, 'Haruki Murakami', 'dfgfdgfd', 'gdfgdfgdfg', 'fgdfg dfgdfgdfg df', 2, 3, '2020-03-09 21:23:50', '2020-03-12 09:35:37'),
(6, 1234567891234, 'La petite Russie', 'asf', 'dsfsdf', 'sdf sd sd fsd sdfsdfsdfsd sdf sdf sd fsd  s', 1, 2, '2020-03-09 21:42:49', '2020-03-12 09:35:37');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`, `is_admin`) VALUES
(1, 'toto', '1234', '2020-03-27 09:47:53', 0),
(2, 'admin', 'admin', '2020-04-11 14:39:32', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
