<?php
include_once('include/includes.php');

// erreur de validation
$username_err = null;
$password_err = null;
$password_err = null;

// Algo SHA256
$hash = "d04b98f48e8f8bcc15c6ae5ac050801cd6dcfd428fb5f9e65c4e16e7807340fa";

if (!empty($_POST)) {

  // les valeur post
  $pUsername = $_POST['username'];
  $pPassword = $_POST['password'];
  $pConfirmPassword = $_POST['confirm_password'];

  // validate input
  $valid = true;
  if (empty($pUsername)) {
    $username_err = 'Champs nom d\'utilisateur  requis';
    $valid = false;
  }

  if (empty($pPassword)) {
    $password_err = 'Champs mot de passe requis';
    $valid = false;
  }

  if (empty($pConfirmPassword) && $pConfirmPassword == $pPassword) {
    $confirm_password_err = 'Veuillez entrer un mot de passe identique';
    $valid = false;
  }

  $pPassword = trim($pPassword);
  
  if (strlen($pPassword) < 8) {
    $password_err = 'Le mot de passe doit contenir au moins 8 caractères';
    $valid = false;
  } 

  else if (!preg_match("/[A-Z]/", $pPassword)) {
    $password_err = 'Le mot de passe doit contenir au moins une lettre majuscule';
    $valid = false;
  }

  // verify connexion
  if ($valid) {
    $pPassword = crypt($pPassword, $hash);
    insertUser($pUsername, $pPassword);
    header("Location: mon-dossier/index.php");
  }
}
?>
<main class="container">
  <h1>Créer un compte</h1>
  <form class="form" action="<?= htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
    <div class="form-group <?= (!empty($usernpConfirmPasswordame_err)) ? 'has-error' : ''; ?>">
      <label>Nom d'utilisateur</label>
      <input type="text" name="username" class="form-control" value="<?= !empty($username); ?>">
      <span class="help-block"><?= !empty($username_err); ?></span>
    </div>
    <div class="form-group <?= (!empty($password_err)) ? 'has-error' : ''; ?>">
      <label>Mot de passe</label>
      <input type="password" name="password" class="form-control" value="<?= !empty($password); ?>">
      <span class="help-block"><?= !empty($password_err); ?></span>
    </div>
    <div class="form-group <?= (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
      <label>Confirmer le mot de passe</label>
      <input type="password" name="confirm_password" class="form-control" value="<?= !empty($confirm_password); ?>">
      <span class="help-block"><?= !empty($confirm_password_err); ?></span>
    </div>
    <div class="form-group">
      <input type="submit" class="btn btn-primary" value="Soumettre">
    </div>
    <p>Vous avez déjà un compte ? <a href="login.php">Connectez vous ici</a>.</p>
  </form>
</main>

<?php include_once('include/components/footer.php');  ?>