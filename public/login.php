<?php
/*
Source de référence pour le système d'authentification :
https://www.tutorialrepublic.com/php-tutorial/php-mysql-login-system.php
*/

include_once('include/includes.php');

$connexion_err = $username_err = $password_err =
  $pUsername = $pPassword = null;
$data = array();


// Algo SHA256
$hash = "d04b98f48e8f8bcc15c6ae5ac050801cd6dcfd428fb5f9e65c4e16e7807340fa";

if (!empty($_POST)) {

  // Données pour la tentative de connexion
  $data['nomUtil'] = null;
  $data['date'] = date('Y-m-d H:i:s', time());
  $data['statut'] = null;

  // keep track post values
  $pUsername = $_POST['username'];
  $pPassword = $_POST['password'];

  // validate input
  $valid = true;
  if (empty($pUsername)) {
    $username_err = "Nom d'utilisateur  requis";
    $valid = false;
  }

  if (empty($pPassword)) {
    $password_err = 'Mot de passe requis';
    $valid = false;
  }

  // Si tous les champs sont valides, on vérifie la connexion
  if ($valid) {
    //Est-ce que l'utilisateur existe?
    if (isUser($pUsername)) {
      $pPassword = crypt($pPassword, $hash);
      $user = isLog($pUsername, $pPassword);
      if ($user) {
        session_start();

        // Mettres les données d'utilisateur dans la session
        // pour authentification sur les pages protégées
        $_SESSION['loggedin'] = true;
        $_SESSION['id'] = $user['id'];
        $_SESSION['username'] = $user['username'];

        $data['nomUtil'] = $user['username'];
        $data['statut'] = "Reussite";
        ajoutConnexion($data);

        // Vérification du rôle d'admin         
        if ($user['is_admin']) {
          $_SESSION['is_admin'] = true;
          header("Location: mon-dossier/index.php");
        } else {
          $_SESSION['is_admin'] = false;
          header("Location: mon-dossier/index.php");
        }
      } else {
        $connexion_err = "Mauvaise combinaison identifiant et mot de passe";
        $data['nomUtil'] = $pUsername;
        $data['statut'] = "Echec";
        ajoutConnexion($data);
      }
    } else {
      $connexion_err = "Mauvaise combinaison identifiant et mot de passe";
      $data['nomUtil'] = $pUsername;
      $data['statut'] = "Echec";
      ajoutConnexion($data);
    }
  }
}
?>


<main class="container">
  <h1>Connexion</h1>

  <form class="form" action="<?= htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
    <div class="form-group">
      <span class="help-block"><?= $connexion_err; ?></span>
    </div>
    <div class="form-group <?= !empty($username_err) ? 'has-error' : ''; ?>">
      <label>Nom d'utilisateur</label>
      <input type="text" name="username" class="form-control" value="<?= $pUsername; ?>">
      <span class="help-block"><?= $username_err; ?></span>
    </div>
    <div class="form-group <?= !empty($password_err) ? 'has-error' : ''; ?>">
      <label>Mot de passe</label>
      <input type="password" name="password" class="form-control">
      <span class="help-block"><?= $password_err; ?></span>
    </div>
    <div class="form-group">
      <input type="submit" class="btn btn-primary" value="Connexion">
    </div>
    <p>Vous n'avez pas de compte dépanneur? <a href="register.php">Créez-en un ici</a>.</p>
  </form>
</main>

<?php include_once('include/components/footer.php');  ?>