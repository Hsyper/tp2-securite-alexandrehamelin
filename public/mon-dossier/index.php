<?php
$root = "../";
include_once $root . 'include/includes.php';

session_start();
if (!isset($_SESSION['loggedin'])) {
  header('location: ../login.php');
}
?>


<!-- Contenu de la page -->
<nav class="container mt-4 ">
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" href="index.php">Mes emprunts</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Mes réservation</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Frais de retard</a>
    </li>
  </ul>
</nav>

<main id="gestion-livres" class="container">

  <div class="flex justify-content-between">
    <h2>Mes emprunts</h2>
  </div>

  <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolorem, dolore esse eos molestias atque saepe odit exercitationem soluta quaerat iure delectus, tempore explicabo sit pariatur recusandae amet asperiores nihil est!</p>
</main>

<?php include_once  $root . 'include/components/footer.php'; ?>