<?php

function database(){
    $db = new Mysqlidb(_HOST, _USER, _PASS, _DB);
    return $db;
}

/********************************************************************/
/***************************** LIVRES  *****************************/
/******************************************************************/

/**** Admin ****/

function getAllLivres()
{
    $db = database();
    $db->orderBy("titre", "asc");
    return $db->get('livres');
}

function storeLivre($data)
{
    $db = database();
    $livre = array(
        'isbn' => $data['isbn'],
        'titre' => $data['titre'],
        'nomAuteur' => $data['nomAuteur'],
        'prenomAuteur' => $data['prenomAuteur'],
        'description' => $data['description'],
        'categorie' => $data['categorie'],
        'genre' => $data['genre']
    );

    $db->insert('livres', $livre);
}

/**** Site ****/

function getLivre($id)
{
    $db = database();
    $db->where('id', $id);
    return $db->get('livres');
}


function storeCommentaire($data)
{
    $db = database();
    $commentaire = array(
        'livre_id' => $data['id'],
        'commentaire' => $data['commentaire'],
        'nom' => $data['nom'],
        'prenom' => $data['prenom']
    );

    $db->insert('commentaires', $commentaire);
}

function getCommentaires($id)
{
    $db = database();
    $db->where('livre_id', $id);
    return $db->get('commentaires');
}


/********************************************************************/
/************************* Authentification ************************/
/******************************************************************/


function insertUser($username, $password)
{
    $db = database();
    $data = array(
        'username' => $username,
        'password' => $password
    );
    $db->insert('users', $data);
}

function isUser($username)
{
    $db = database();
    $db->where('username', $username);
    $user = $db->get('users');
    return $user;
}


function isLog($username, $password)
{
    $db = database();
    $db->where('username', $username);
    $db->where('password', $password);
    $user = $db->get('users');
    if ($user) {
        return $user[0];
    } else {
        return null;
    }
}

function ajoutConnexion($data)
{
    $db = database();
    $data = array(
        'nomUtil' => $data['nomUtil'],
        'date' => $data['date'],
        'statut' => $data['statut']
    );
    $db->insert('connexions', $data);
}

function getAllConnexions() 
{
    $db = database();
    return $db->get('connexions');
}
