<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">

<head>
  <meta charset="utf-8" />

  <!-- Responsive -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville|Source+Sans+Pro:400,600" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
    crossorigin="anonymous" />
  
  <!-- CSS -->
  <link media="screen" rel="stylesheet" href="<?=$root;?>css/main.css" />
  <link media="screen" rel="stylesheet" href="<?=$root;?>css/owl.carousel.min.css" />
  <link media="screen" rel="stylesheet" href="<?=$root;?>css/owl.theme.default.min.css" />
</head>

<body>

  <!-- HEADER -->
  <header>
    <!-- MENU MOBILE -->
    <div id="modale-menu" tabindex="-1" role="navigation" class="modale-container hidden">
      <div data-activate="modale-menu" class="overlay activator"></div>
      <nav data-activate="modale-menu" class="modale-menu activator" role="navigation">
        <ul>
          <li>
            <a href="<?=$root;?>catalogue">Catalogue</a>
          </li>
          <li>
            <a href="<?=$root;?>activites">Activités</a>
          </li>
          <li>
            <a href="<?=$root;?>actualites">Actualités</a>
          </li>
          <li>
            <a href="<?=$root;?>mon-dossier/index" class="call-to-action"><i class="fas fa-user-circle"></i> Mon dossier</a>
          </li>
        </ul>
      </nav>
    </div>

    <!-- MENU DESKTOP -->
    <div id="header" class="container clearfix">
      <div class="flex header-flex">
        <!-- LOGO -->
        <h1>
          <a href="<?=$root;?>index">
            <img src="<?=$root;?>img/LOGO.svg">
          </a>
        </h1>
        <!-- (caché) image MENU BURGER (activateur) -->
        <div id="menu-burger" class="menu-burger fr laptop-hidden">
          <img data-activate="modale-menu" class="activator" src="<?=$root;?>img/varia/menu-burger.svg" >
        </div>
        <!-- NAVIGATION PRINCIPALE -->
        <div id="menu-complet" class="menu-complet laptop clearfix">
          <nav id="main-nav" class="main-nav" role="navigation">
            <ul class="inline-block">
              <li>
                <a href="<?=$root;?>catalogue">Catalogue</a>
              </li>
              <li>
                <a href="<?=$root;?>activites">Activités</a>
              </li>
              <li>
                <a href="<?=$root;?>actualites">Actualités</a>
              </li>
              <li>
                <a href="<?=$root;?>mon-dossier/index" class="bouton call-to-action"><i class="fas fa-user-circle"></i> Mon dossier</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>

  </header>
  <!-- fin du header -->