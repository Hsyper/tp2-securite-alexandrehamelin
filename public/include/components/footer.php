  <!-- FOOTER -->
  <footer class="clearfix">
    <div class="container">
      <div class="row">
        <div class="col">
          <!-- LOGO -->
          <a href="index.html">
            <img src="<?=$root;?>img/LOGO.png">
          </a>
          <div class="soc-med">
            <ul class="inline-block">
              <li>
                <a href="https://www.facebook.com" target="_blank">
                  <i class="fab fa-facebook"></i>
                </a>
              </li>
              <li>
                <a href="https://twitter.com" target="_blank">
                  <i class="fab fa-twitter-square"></i>
                </a>
              </li>
              <li>
                <a href="https://www.youtube.com" target="_blank">
                  <i class="fab fa-youtube-square"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col">
          <h3>Navigation</h3>
          <nav id="footer-nav" class="footer-nav">
            <ul>
              <li>
                <a href="<?=$root;?>catalogue.html">Catalogue</a>
              </li>
              <li>
                <a href="<?=$root;?>activites.html">Activités</a>
              </li>
              <li>
                <a href="<?=$root;?>actualites.html">Actualités</a>
              </li>
              <li>
                <a href="<?=$root;?>contact.html">Nous joindre</a>
              </li>
              <li>
                <a href="#">Mon dossier</a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="col">
          <h3>À propos</h3>
          <nav class="footer-nav">
            <ul>
              <li>
                <a href="#">Nous joindre</a>
              </li>
              <li>
                <a href="#">Emploi et bénévolat</a>
              </li>
              <li>
                <a href="#">Ville de Berthierville</a>
              </li>
              <li>
                <a href="#">Maison de la littérature</a>
              </li>
              <li>
                <a href="#">Vision du développement</a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="col">
          <h3 id="infolettreTitle">Infolettre</h3>
          <p id="infolettreDesc">Pour rester informé des activités, nouveautés et actualités, abonnez-vous à notre
            infolettre :</p>
          <form action="#" method="POST">
            <div class="flex input-flex">
              <input type="email" id="champInfolettre" name="champInfolettre" placeholder="Votre courriel">
              <input type="submit" class="bouton bouton-ghost" value="Envoyer">
            </div>
          </form>
        </div>
      </div>
    </div>
  </footer>
  <!-- fin du FOOTER -->

  <!-- SCRIPTS -->
  <script type="text/javascript" src="<?=$root;?>script/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="<?=$root;?>script/owl.carousel.min.js"></script>
  <script type="text/javascript" src="<?=$root;?>script/main.js"></script>

</body>

</html>