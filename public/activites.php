<?php 
include_once('include/includes.php');
?>
  <!-- MAIN -->
  <main id="content">

    <!-- Activités -->
    <div id="activites" class="container">

      <h1>Activités</h1>

      <div>
        <a href="#" class="current bouton bouton-ghost pt-btn mot-clef">
          <i class="fas fa-plus-circle"></i>
          Toutes les activités
        </a>
        <a href="#" class="bouton bouton-ghost pt-btn mot-clef">
          <i class="fas fa-smile-beam"></i>
          Activités jeunesse
        </a>
        <a href="#" class="bouton bouton-ghost pt-btn mot-clef">
          <i class="fas fa-pencil-ruler"></i>
          Ateliers et animations
        </a>
        <a href="#" class="bouton bouton-ghost pt-btn mot-clef">
          <i class="fas fa-film"></i>
          Cinéma
        </a>
        <a href="#" class="bouton bouton-ghost pt-btn mot-clef">
          <i class="fas fa-theater-masks"></i>
          Spectacles
        </a>
        <a href="#" class="bouton bouton-ghost pt-btn mot-clef">
          <i class="fas fa-comments"></i>
          Conférences et rencontres
        </a>
        <a href="#" class="bouton bouton-ghost pt-btn mot-clef">
          <i class="fas fa-palette"></i>
          Expositions
        </a>
        <a href="#" class="bouton bouton-ghost pt-btn mot-clef">
          <i class="fas fa-laptop"></i>
          Technos
        </a>
        <a href="#" class="bouton bouton-ghost pt-btn mot-clef">
          <i class="fas fa-trophy"></i>
          Concours
        </a>
      </div>

      <div >

        <section id="activites" >

          <div class="actu-card  row ">
            <div class="col-sm-6 img-container ">
              <img src="img\acti\halloween.PNG">
            </div>
            <div class="col-sm-6">
              
              <h3 class="no-margin">Décoration de citrouille pour tout âge</h3>
              <div>
                <strong>Date :</strong> Jeudi, le 24 octobre 2019
              </div>
              <div>
                <strong>Coût :</strong> 7,50$
              </div>
              <p> Lorem ipsum dolor sit amet, mel no option quaerendum, quando malorum et eum. Vidisse minimum
                  inimicus usu et. Eros cetero voluptaria cu vis, oblique eruditi maiestatis vis ex. Te vim saperet 
                  deseruisse.An pri imperdiet quaerendum.</p>
              <a href="#" class="lireSuite">Lire la suite <i class="fas fa-plus-circle"></i></a>
            </div>
          </div>

          <div class="actu-card row  ">
            <div class="col-sm-6 img-container ">
              <img src="img\acti\laura-ipsum.PNG">
            </div>
            <div class="col-sm-6">
              
              <h3 class="no-margin">Discussion avec l'auteure Laura Ipsum</h3>
              <div>
                <strong>Date :</strong> Mardi, le 26 novembre 2019
              </div>
              <div>
                <strong>Coût :</strong> Gratuit
              </div>
              <p> Lorem ipsum dolor sit amet, mel no option quaerendum, quando malorum et eum. Vidisse minimum
                  inimicus usu et. Eros cetero voluptaria cu vis, oblique eruditi maiestatis vis ex. Te vim saperet 
                  deseruisse.An pri imperdiet quaerendum.</p>
              <a href="#" class="lireSuite">Lire la suite <i class="fas fa-plus-circle"></i></a>
            </div>
          </div>

          <div class="actu-card  row ">
            <div class="col-sm-6 img-container ">
              <img src="img\acti\lancement-saison-19.PNG">
            </div>
            <div class="col-sm-6">
              
              <h3 class="no-margin">Lancement du calendrier 2020</h3>
              <div>
                <strong>Date :</strong> Vendredi, le 12 décembre 2019
              </div>
              <div>
                <strong>Coût :</strong> Gratuit
              </div>
              <p> Lorem ipsum dolor sit amet, mel no option quaerendum, quando malorum et eum. Vidisse minimum
                  inimicus usu et. Eros cetero voluptaria cu vis, oblique eruditi maiestatis vis ex. Te vim saperet 
                  deseruisse.An pri imperdiet quaerendum.</p>
              <a href="#" class="lireSuite">Lire la suite <i class="fas fa-plus-circle"></i></a>
            </div>
          </div>



          <div class="pagination-custom">
            <a href="#"><i class="fas fa-angle-left"></i></a>
            <a href="#" class="bouton numeroPage bouton-mauve">1</a>
            <a href="#" class="bouton numeroPage bouton-ghost">2</a>
            <a href="#" class="bouton numeroPage bouton-ghost">3</a>
            <a href="#" class="bouton numeroPage bouton-ghost">4</a>
            <a href="#" class="bouton numeroPage bouton-ghost">5</a>
            <a href="#" class="bouton numeroPage bouton-ghost">6</a>
            <a href="#" class="bouton numeroPage bouton-ghost">7</a>
            <a href="#" class="bouton numeroPage bouton-ghost">8</a>
            <a href="#" class="bouton numeroPage bouton-ghost">9</a>
            <a href="#" class="bouton numeroPage bouton-ghost">10</a>
            <a href="#"><i class="fas fa-angle-right"></i></a>
          </div>
          <!-- fin du flex-pagination -->
        </section>

      </div>
      <!-- fin du flex-row -->
    </div>
    <!-- fin du container -->

  </main>
  <!-- fin du Main -->

<?php include_once('include/components/footer.php');  ?>