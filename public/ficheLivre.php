<?php
include_once('include/includes.php');

// Id du livre
$data = array();
$id =  $_GET['id_livre'];
$livres = getLivre($id);
$livre = $livres[0];

if (!empty($_POST)) {
  // Valeurs du post
  $data['nom'] = trim($_POST['nom']);
  $data['prenom'] = trim($_POST['prenom']);
  $data['commentaire'] = trim($_POST['commentaire']);
  $data['id'] = $id;

  // Gestion des erreurs
  $error = array();

  if (empty($data['nom'])) {
    $error['nom'] = 'Un nom est requis';
  }
  if (empty($data['prenom'])) {
    $error['prenom'] = 'Un prénom est requis';
  }
  if (empty($data['commentaire'])) {
    $error['commentaire'] = 'Le commentaire est requis';
  }

  if (empty($error)) {
    // Pour prévenir la faille XSS
    $data['nom'] = htmlspecialchars ($data['nom'], ENT_QUOTES);
    $data['nom'] = htmlspecialchars ($data['nom'], ENT_SUBSTITUTE);
    $data['prenom'] = htmlspecialchars ($data['prenom'], ENT_QUOTES);
    $data['prenom'] = htmlspecialchars ($data['prenom'], ENT_SUBSTITUTE);
    $data['commentaire'] = htmlspecialchars ($data['commentaire'], ENT_QUOTES);
    $data['commentaire'] = htmlspecialchars ($data['commentaire'], ENT_SUBSTITUTE);
    
    // Ajouter le commentaire
    storeCommentaire($data);
    
    // Réinitialisation du POST
    unset($_POST);
    $_POST = array();
    $data = array();
  }
}

// Récupérer tous les commentaires du livre
$commentaires = getCommentaires($id);

?>

<!-- Contenu de la page -->
<main id="content">

  <!-- SECTION Breadcrumb -->
  <nav id="breadcrumb" class="container">
    <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="catalogue.php">Catalogue</a></li>
      <li class="breadcrumb-item"><a href="catalogue.php">Adulte</a></li>
      <li class="breadcrumb-item"><a href="catalogue.php">Bande-dessinée</a></li>
      <li class="breadcrumb-item"><span class="bold">Moi, ce que j'aime, c'est les monstres</span></li>
    </ul>

  </nav>

  <!-- SECTION fiche de livre -->
  <section id="ficheDeLivre" class="container">
    <h1><?= $livre['titre'] ?></h1>
    <div class="flex row">
      <section id="couverture" class="col-md-4 img-container">
        <?php
        $img = "img/livres/cover" . $livre['id'] . ".jpg";
        if (file_exists($img)) : ?>
          <img src="<?= $img; ?>">
        <?php else : ?>
          <img src="img/livres/cover.jpg">
        <?php endif; ?>
        <table>
          <tr>
            <th>ISBN :</th>
            <td><?= $livre['isbn'] ?></td>
          </tr>
          <tr>
            <th>Auteur :</th>
            <td><a href="#" class="auteur"><span class="nomAuteur"><?= $livre['nomAuteur'] ?></span>,
                <?= $livre['prenomAuteur'] ?></a></td>
          </tr>
          <tr>
            <th>Publication :</th>
            <td>Alto, 2018</td>
          </tr>
          <tr>
            <th>Titre original :</th>
            <td><?= $livre['titre'] ?></td>
          </tr>
          <tr>
            <th>Traduction :</th>
            <td><a href="#" class="auteur"><span class="nomAuteur">Khalifa</span>,
                Jean-Charles</a></td>
          </tr>
          <tr>
            <th>Nbr. de pages :</th>
            <td>416</td>
          </tr>
          <tr>
            <th>Genre :</th>
            <td><a href="#" class="bleu">Bande-dessinée adulte</a></td>
          </tr>
        </table>
        <h2>Mots clés</h2>
        <div class="pad-x-1-asynch">
          <a href="#" class="bouton bouton-ghost pt-btn mot-clef">Monstres</a>
          <a href="#" class="bouton bouton-ghost pt-btn mot-clef">Meurtre</a>
          <a href="#" class="bouton bouton-ghost pt-btn mot-clef">Enquête</a>
          <a href="#" class="bouton bouton-ghost pt-btn mot-clef">Journal intime</a>
          <a href="#" class="bouton bouton-ghost pt-btn mot-clef">Bande dessinée</a>
          <a href="#" class="bouton bouton-ghost pt-btn mot-clef">Roman graphique</a>
          <a href="#" class="bouton bouton-ghost pt-btn mot-clef">Chicago</a>
        </div>
        <a href="#" class="bouton bouton-bleu pt-btn">Réserver ce livre</a>
      </section>
      <div class="col-md-6">
        <section id="descriptionLivre">
          <h2>Résumé</h2>
          <p><?= $livre['description'] ?></p>
          <a href="#" class="bouton bouton-bleu pt-btn">Réserver ce livre</a>
        </section>
        <section id="commentaires">
          <h2>Laissez un commentaire sur ce livre</h2>
          <?php if (!empty($error)) : ?>
            <div class="alert alert-danger" role="alert">
              Veuillez corriger les erreurs suivantes
            </div>
          <?php endif; ?>
          <form class="std-form pb-3" action="" method="POST">
            <div>
              <label for="prenom">Prénom</label>
              <input type="text" id="prenom" name="prenom" value="<?= !empty($data['prenom']) ? $data['prenom'] : ''; ?>">
              <?php if (!empty($error['prenom'])) : ?>
                <div>
                  <span class="error"><?= $error['prenom']; ?></span>
                </div>
              <?php endif; ?>
            </div>
            <div>
              <label for="nom">Nom</label>
              <input type="text" id="nom" name="nom" value="<?= !empty($data['nom']) ? $data['nom'] : ''; ?>">
              <?php if (!empty($error['nom'])) : ?>
                <div>
                  <span class="error"><?= $error['nom']; ?></span>
                </div>
              <?php endif; ?>
            </div>
            <div>
              <label for="commentaire">Commentaire</label>
              <textarea id="commentaire" name="commentaire"><?= !empty($data['commentaire']) ? $data['commentaire'] : ''; ?></textarea>
              <?php if (!empty($error['commentaire'])) : ?>
                <div>
                  <span class="error"><?= $error['commentaire']; ?></span>
                </div>
              <?php endif; ?>
            </div>
            <div class="btn-group">
              <input class="bouton bouton bouton-bleu" type="submit" value="Soumettre">
            </div>
          </form>

          <?php if (!empty($commentaires)) : ?>
            <h2>Commentaires sur ce livre</h2>
            <?php foreach ($commentaires as $com) : ?>
              <div class="border-top pt-2">
                <h3 class="h5"><span class="text-uppercase"><?= $com['nom'] ?></span>, <?= $com['prenom'] ?></h3>
                <p><?= $com['commentaire']; ?></p>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

        </section>
      </div>

    </div>
    <!-- fin du flex -->
  </section>
  <!-- fin du container -->

  <!-- SECTION Suggestions -->
  <section id="suggestions">
    <div class="container">
      <h2>Quelques suggestions pour vous...</h2>
      <div class="flex row catalogue-flex">

        <div class="livre-card col-6 col-md-4 col-lg-3">
          <div class="livre-img img-container">
            <a href="ficheLivre.php">
              <img src="img/livres/cover5.jpg">
            </a>
          </div>
          <div class="livre-infos">
            <a href="ficheLivre.php">
              <h4 class="ellipse">Le meurtre du commandeur</h4>
            </a>
            <p>Par : <a href="#" class="auteur"><span class="nomAuteur">Murakami</span>,
                Haruki</a></p>
            <a href="#" class="bouton bouton-bleu pt-btn">Réserver</a>
          </div>
        </div>

        <div class="livre-card col-6 col-md-4 col-lg-3">
          <div class="livre-img img-container">
            <a href="ficheLivre.php">
              <img src="img/livres/cover6.jpg" alt="Couverture du livre de Franck Desharanais, La petite Russie">
            </a>
          </div>
          <div class="livre-infos">
            <a href="ficheLivre.php">
              <h4 class="ellipse">La petite Russie</h4>
            </a>
            <p>Par : <a href="#" class="auteur"><span class="nomAuteur">Desharnais</span>,
                Franck</a></p>
            <a href="#" class="bouton bouton-bleu pt-btn">Réserver</a>
          </div>
        </div>

        <div class="livre-card col-6 col-md-4 col-lg-3">
          <div class="livre-img img-container">
            <a href="ficheLivre.php">
              <img src="img/livres/cover7.jpg" alt="Couverture du livre de Loredana Drilea et Marc Fisher, La petite orpheline">
            </a>
          </div>
          <div class="livre-infos">
            <a href="ficheLivre.php">
              <h4 class="ellipse">La petite orpheline</h4>
            </a>
            <p class="ellipse">Par : <a href="#" class="auteur"><span class="nomAuteur">Drilea</span>
                , Loredana</a> et <a href="#" class="auteur"><span class="nomAuteur">Fisher</span>
                , Marc</a></p>
            <a href="#" class="bouton bouton-bleu pt-btn">Réserver</a>
          </div>
        </div>

        <div class="livre-card col-6 col-md-4 col-lg-3">
          <div class="livre-img img-container">
            <a href="ficheLivre.php">
              <img src="img/livres/cover8.jpg">
            </a>
          </div>
          <div class="livre-infos">
            <a href="ficheLivre.php">
              <h4 class="ellipse">Encyclopédie du savoir relatif et absolu</h4>
            </a>
            <p>Par : <a href="#" class="auteur"><span class="nomAuteur">Werber</span>,
                Bernard</a></p>
            <a href="#" class="bouton bouton-bleu pt-btn">Réserver</a>
          </div>
        </div>

      </div>
      <!-- fin du flex -->
    </div>
    <!-- fin du container -->
  </section>

</main> <!-- fin Contenu de page -->
<!-- fin du Main -->

<!-- FENÊTRE MODALE -->
<div id="modale-reservation" class="modale-container hidden">
  <div data-activate="modale-reservation" class="overlay activator"></div>
  <div class="fenetre-modale">
    <h4>Réservation</h4>
    <div data-activate="modale-reservation" class="fermer activator"><i class="fas fa-times-circle"></i></div>
    <form action="#" method="POST">
      <div>
        <label>Votre nom:</label>
        <input type="text" id="nom" name="nom">
      </div>

      <div>
        <label>Date de la réservation:</label>
        <input type="date" id="date" name="date">
      </div>

      <div>
        <label>Commentaire (facultatif):</label>
        <textarea rows="4" cols="50" id="commentaire" name="commentaire"></textarea>
      </div>

      <input type="button" class="bouton-fenetre" value="Réserver">
    </form>
  </div>
</div> <!-- Fin FENÊTRE MODALE -->

<?php include 'include/components/footer.php'; ?>