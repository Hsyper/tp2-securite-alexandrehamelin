<?php 

// Valeurs du post
$data['isbn'] = $_POST['isbn'];
$data['titre'] = $_POST['titre'];
$data['nomAuteur'] = $_POST['nomAuteur'];
$data['prenomAuteur'] = $_POST['prenomAuteur'];
$data['description'] = $_POST['description'];
$data['categorie'] = $_POST['categorie'];
$data['genre'] = $_POST['genre'];

// Gestion des erreurs
$error = array();

if (isset($_SESSION['token']) AND isset($_POST['token']) AND !empty($_POST['token']) AND !empty($_SESSION['token'])) {
    if ($_SESSION['token'] == $_POST['token']) {
        $error['token'] == null;
    } else {
        $error['token'] = "Erreur de vérification";
    }
} else {
    $error['token'] = "Erreur de vérification";
}

if (preg_match("/\d{12}(?:\d|X)/", $data['isbn'])==0) {
    $error['isbn'] = 'Un nombre à 13 carcatères est requis';
}
if (empty($data['titre'])) {
    $error['titre'] = 'Un titre est requis';
}
if (empty($data['nomAuteur'])) {
    $error['nomAuteur'] = 'Un nom d\'auteur est requis';
}
if (empty($data['prenomAuteur'])) {
    $error['prenomAuteur'] = 'Un prénom d\'auteur est requis';
}
if (empty($data['description'])) {
    $error['description'] = 'Une description du livre est requise';
}
if (empty($data['categorie'])) {
    $error['categorie'] = 'Une catégorie doit être sélectionnée';
}
if (empty($data['genre'])) {
    $error['genre'] = 'Un genre doit être sélectionné';
}