<?php
$root = "../";
include_once $root . 'include/includes.php';
$livres = getAllLivres();

session_start();
if (!isset($_SESSION['loggedin'])) {
  header('location: ../login.php');
}
if ($_SESSION['is_admin'] != true) {
  header('location: ../login.php');
}
?>


<!-- Contenu de la page -->
<nav class="container mt-4 ">
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" href="index.php">Gestion des livres</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Gestion des utilisateurs</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Journal de connexions</a>
    </li>
  </ul>
</nav>

<main id="gestion-livres" class="container">
  <?php if (!empty($_GET['message'])) :
    if ($_GET['message'] == 'success') : ?>
      <div class="alert alert-success" role="alert">
        Le livre a bien été ajouté
      </div>
    <?php endif; ?>
  <?php endif; ?>

  <div class="flex justify-content-between">
    <h2>Gestion des livres</h2>
    <a href="livreCreate.php" class="btn btn-primary">Ajouter un livre</a>
  </div>


  <?php if (!empty($livres)) : ?>
    <table class="table-livres">
      <thead>
        <tr>
          <th scope="col">Couverture</th>
          <th scope="col">ISBN</th>
          <th scope="col">Titre</th>
          <th scope="col">Auteur</th>
          <th scope="col">Catégorie</th>
          <th scope="col">Genre</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($livres as $livre) : ?>
          <tr>
            <td>
              <?php
              $img = "../img/livres/cover" . $livre['id'] . ".jpg";
              if (file_exists($img)) : ?>
                <img src="<?= $img; ?>">
              <?php else : ?>
                <img src="../img/livres/cover.jpg">
              <?php endif; ?>
            </td>
            <td><?= $livre['isbn'] ?></td>
            <td><?= $livre['titre'] ?></td>
            <td><span class="text-uppercase"><?= $livre['nomAuteur'] . '</span>,' . $livre['prenomAuteur']; ?></td>
            <td><?= $livre['categorie'] ?></td>
            <td><?= $livre['genre'] ?></td>
            <td>
              <ul class="list-actions">
                <li><a href="../ficheLivre.php" class="">Voir sur le site</a></li>
                <li><a href="#" class="">Modifier</a></li>
                <li><a href="#" class="text-danger">Supprimer</a></li>
              </ul>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
</main>

<?php include_once  $root . 'include/components/footer.php'; ?>