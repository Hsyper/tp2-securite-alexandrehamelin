<?php
$root = "../";
include_once $root . 'include/includes.php';

session_start();
if (!isset($_SESSION['loggedin'])) {
  header('location: ../login.php');
}
if ($_SESSION['is_admin'] != true) {
  header('location: ../login.php');
}
?>

<!-- Contenu de la page -->
<main id="content">
    <h1 style="text-align:center">Journalisation des connexions au site</h1>
    <table class="table table-striped table- table-hover">
        <thead class="thead-dark">
            <tr class="h3">
                <th scope="col">Id connexion</th>
                <th scope="col">Utilisateur</th>
                <th scope="col">Heure et date</th>
                <th scope="col">Statut</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $data = array();
                $data = getAllConnexions();
                foreach ($data as $element) {
                    echo '<tr class="h5">';
                    echo '<td scope="row">' . $element['id'] . '</td>';
                    echo '<td>' . $element['nomUtil'] . '</td>';
                    echo '<td>' . $element['date'] . '</td>';
                    echo '<td>' . $element['statut'] . '</td>';
                    echo '</tr>';
                }
            ?>
        </tbody>
    </table>
</main>

<?php include '../include/components/footer.php'; ?>