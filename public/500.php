<?php 
include_once('include/includes.php');
?>

<main style="text-align: center;">
    <h1>Impossible d'accèder à la ressource</h1>
    <h3><a href="/public/index.php">Retour à l'accueil</a></h3>
</main>

<?php include_once('include/components/footer.php');  ?>
